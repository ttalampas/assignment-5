#include <iostream>
#include <string>

using namespace std;

class FootballPlayer { // player name and points scored 
private:
    // TODO name, points 
    string name;
    int points; 
public:
    FootballPlayer(const string &name);
    const string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};

