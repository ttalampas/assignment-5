#include "footballteam.hpp"


struct FootballTeamStats {
    int score;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame { // get player and points scored during game (stats)
private:
    // TODO player, score
    
public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);
    void recordScore(Side side, const string& playerName, int points);
    FootballGameStats getStats() const;
};
