#include "footballplayer.hpp"

const int MAX_PLAYERS = 100;

class FootballTeam { // football team name and players on team
private:
    // TODO team name, player name
    string tname;
    int tsize; 
    FootballPlayer **players; 
public:
    FootballTeam(const string &name);
    ~FootballTeam();
    void addPlayer(const string &name); //wtf do i use this for 
    FootballPlayer *getPlayer(const string &name) const; 
};

