#include "footballplayer.hpp"

// TODO: FootballPlayer methods 
FootballPlayer::FootballPlayer(const string &name) 
:name(""), points(0) { // initialize name and points 
    this->name = name;
    this->points = 0;
}

//functions to return name and points 
const string &FootballPlayer::getName() const {
    return name; 
}

int FootballPlayer::getPointsScored()  const {
    return points; 
}

void recordScored(int points) {
    points = points; 
}
