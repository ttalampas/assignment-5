# Assignment 5: Football Scores

In honor of Super Bowl 57, write a program that generates a report of an American football game. It will take as input a file containing the statistics for a single game, then print a report on the game showing the final scores and the highest-scoring player for each team.

## File Format

The statistics file will contain comma-separated values in the following format:

* Line 1: Home team name
* Line 2: Comma-separated list of home team players
* Line 3: Visiting team name
* Line 4: Comma-separated list of visiting team players
* Lines 5+: Comma-separated score statistic in the format "SIDE, NAME, SCORE", where "SIDE" is either "H" (home team) or "V" (visitors), name is the name of the player who scored, and "SCORE" is one of:
    * TD = touchdown (6 points)
    * FG = field goal (3 points)
    * S = safety (2 points)
    * EP = extra point (1 point)

## Example

For example, assume a file called `superbowl.csv` contains:

    Kansas City Chiefs
    Skyy Moore,Kadarius Toney,Harrison Butker
    Philadelphia Eagles
    DeVonta Smith,Zach Pascal,Jake Elliott
    H,Kadarius Toney,TD
    H,Harrison Butker,EP
    V,DeVonta Smith,S

You can then run your program to generate a report for this game:

    $ ./football superbowl.csv
    Home: 7
    Visitors: 2
    Winner: Home
    Highest scorers:
      Home: Kadarius Toney (6 points)
      Visitors: DeVonta Smith (2 points)

## Class Definitions

Start your implementation with the class definitions in [football.cpp](https://gitlab.com/vocaro/assignment-5/-/blob/main/football.cpp). Your assignment is to implement the rest of the code.

## Additional Requirements

* For lists of data, use dynamically allocated arrays, not vectors.
* All attributes should be private.
* Program-defined types should be allocated dynamically. For instance, if you want to create a variable of type `FootballGame`, create the variable as a pointer and allocate a `FootballGame` instance using new. Built-in types such as `int` and `string` can be instances instead of pointers.
* The program must check for errors such as invalid football scores (e.g., 5) in both the input file and function parameters.
* The main function should return 0 if successful, otherwise 1.
* You can assume the structure of the input file is valid (e.g. commas will be in the right place), but you cannot assume that the data is valid. For instance, a name attached to a score may not exist, or the score may be invalid.
* If a team scored no points, then print "none" instead of a player name. If more than one player scored the most points, choose only one name to print.
* You can assume the number of players will never be more than `MAX_PLAYERS`.
* Do not perform any calculations in `FootballFileParser::printStats`. It should get the statistics from `FootballGame::getStats` and simply print them.

## Hints

* In this assignment, it is not necessary to:
    * Check for nullptr when invoking new in a constructor
    * Set pointers to nullptr after deleting them in a destructor
* In destructors, if you have an array of pointers, remember to delete the pointer elements as well as the array itself.
* `FootballGame` should contain pointers to two dynamically allocated `FootballTeam` objects.
* `FootballTeam` should contain a dynamically allocated array of dynamically allocated `FootballPlayer` objects (`FootballPlayer **players`).
* Before submitting, use your favorite memory leak tool to confirm there are no leaks.
* Use a `switch` statement when checking a `Side` value.
* Feel free to add additional functions to the classes. This is not strictly necessary but may help avoid duplicate code and improve structure. Remember to make functions public only if they are needed outside the class. Otherwise, they should be private.
* The `FootballTeam` class should contain a dynamically allocated array of dynamically allocated `FootballPlayer` instances (e.g. `FootballPlayer **_players`). Determining the size of this list can be done in a couple of different ways:
    * In `FootballTeam`'s constructor, allocate an array of size `MAX_PLAYERS` and use a separate attribute (e.g. `int _numPlayers`) to keep track of how many player instances have been allocated. This is the same approach we discussed in [Lab 10: Destructors](https://clpccd.instructure.com/courses/33834/pages/lab-10-destructors?module_item_id=2773472).
    * Another approach is to make two passes through the file: An initial pass only to count the total number of players, and a second pass to actually process the data. This is the same approach shown in [Lab 6: Returning Pointers from Functions](https://clpccd.instructure.com/courses/33834/pages/lab-6-returning-pointers-from-functions?module_item_id=2765185) ("good" version).
    * Both approaches are valid and you can choose whichever one you like best. The first one runs faster because it only needs to process the file once, but it wastes some memory (unused pointers) and places a compile-time limit on the maximum players. The second approach requires more code and is a little slower because of the extra pass through the file, but it is more efficient with memory and does not place an artificial bound on the number of players.

If you need additional help, add comments to this assignment, start a discussion in the Discussions section of Canvas, or email your teacher. Please do not get help from other students (unless you are discussing openly in Canvas). The work should be yours alone.

## Submission

Your submission should be a merge request for Dr. Harmon's [assignment-5 repository](https://gitlab.com/vocaro/assignment-5). The merge request should modify `football.cpp` so that it contains all of the code needed to build and run. (Don't add additional files.)

Don't forget to submit a link to your merge request through the Canvas assignment page before the deadline.

## Grading

This assignment is worth 60 points:

* *Player class: 10 points.* All functions should be implemented correctly.
* *Team class: 10 points.* All functions should be implemented correctly.
* *Game class: 10 points.* All functions should be implemented correctly.
* *Parser class: 10 points.* All functions should be implemented correctly.
* *Error handling: 10 points.* Correctly handle out-of-bounds array access, invalid function params (especially invalid football scores), file parsing I/O errors, wrong number of command line parameters, and so on. (Checking for nullptr with the new operator is only necessary outside of constructors.)
* *Code quality: 10 points.* Use appropriate comments, function decomposition, and naming. There should be no memory leaks.

Good luck!
