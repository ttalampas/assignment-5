#include "footballstats.hpp"
#include <fstream>


class FootballFileParser {
private:
    // TODO
    string filename;
public:
    FootballFileParser(const string &filepath);
    bool read();  // Returns false if parsing fails
    void printStats() const;
};

